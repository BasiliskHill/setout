/*
    SetOut.h - Library for quickly setting digital outputs
    Created by Hunter T. Hill, 4th December, 2018.
    Released to public domain
*/

#ifndef SetOut_h
#define SetOut_h

#include "Arduino.h"

class SetOut 
{
    public:
        SetOut(const int[55], int);
        void values(int intIn);
    private:
        int _pins[55];
        int _pinsSize;
};

#endif