#	SetOut
##	An easy Arduino library meant to replicate a command from Pickaxe

The library only has its constructor and a single public method.

To construct a new SetOut object simply write ``SetOut objectName(pinsArray, pinsSize);``

To then use the functionality of the library all you need to do is enter a value into ``objectName.values(int);``
values() expects an int, and turns it into a binary code to send to the pins shown in pinsArray, 0 is low, 1 is high.
In order to give a binary value to the method, you only need to add the prefix ``0b`` to the int to signify it is a binary number and Arduino will handle the rest

NOTE: values() will asign values from the least significant bit to the most significant.
