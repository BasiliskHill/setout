/*
    SetOut.cpp - Library for quickly setting digital outputs
    Created by Hunter T. Hill, 4th December, 2018.
    Released to public domain
*/

//  Include headers
#include "Arduino.h"
#include "SetOut.h"
//  Constructor
SetOut::SetOut(const int pins[], int pinsSize) {
    //  Loop over pins array
    for(int i = 0; i < pinsSize; i++) {
        //  Globalise array
        _pins[i] = pins[i];
    }
	//	Globalise array size
	_pinsSize = pinsSize;
    
}
//  Values function
void SetOut::values(int intIn) {
  //  Loop over the binary
  for(int i = 0; i < _pinsSize; i++) {
        //  Take lest significant bit out of int
        int val = bitRead(intIn, i);
        //  Write it as HIGH or LOW
        if(val == 1) digitalWrite(_pins[i], HIGH);
        else if(val == 0) digitalWrite(_pins[i], LOW);
  }
}