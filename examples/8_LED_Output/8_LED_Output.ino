//  Include the library in the sketch
#include <SetOut.h>
//  Create the pins array with max size of 55 (0-53 Mega pins +1)
const int pins[55] = { 2,3,4,5,6,7,8,9 };
//  Create the setOut object, passing in the pins to use and the array size
SetOut setOut(pins, 8); //  pinsArray, pinsSize

void setup() {
}

void loop() {
  //  Turn on the first and third LEDs with an integer equal to binary 101
  setOut.values(5);
  //  Delay by a second to look at it
  delay(1000);
  //  Turn on both the first and second LEDs with binary
  setOut.values(0b11);
  //  Delay
  delay(1000);
  //  Turn on all 8 LEDs with an integer equal to binary 11111111
  setOut.values(255);
  //  Delay
  delay(1000);
  //  Turn on every second LED with binary
  setOut.values(0b01010101);
  //  Delay before restarting the loop
  delay(1000);  
}



